from django.contrib import admin
from .models import *


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ['user', 'medical_code', 'medical_expertise']
    # readonly_fields = ['created_at', 'updated_at']
