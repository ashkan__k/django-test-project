from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Doctor(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name='doctor', verbose_name='کاربر')
    medical_code = models.CharField(max_length=255, verbose_name='کد پزشکی', null=True, blank=True)
    medical_expertise = models.CharField(verbose_name='تخصص', max_length=255, null=True, blank=True)
    description = models.TextField(max_length=500, verbose_name='توضیحات', null=True, blank=True)

    class Meta:
        verbose_name = 'پروفایل دکتر'
        verbose_name_plural = 'پروفایل دکتر ها'

    def __str__(self):
        return f"{self.user}"