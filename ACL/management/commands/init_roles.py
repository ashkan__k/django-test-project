from django.core.management import BaseCommand
from ACL.models import *
from ACL.permissions import PERMISSIONS, ROLE_CODES

DOCTOR_PERMS = [
    'appointment_list',
    'appointment_create',
    'appointment_edit',
    'appointment_delete',

    'appointment_list',
    'appointment_create',
    'appointment_edit',
    'appointment_delete',
]

STAFF_PERMS = [
    'appointment_list',
    'appointment_create',
    'appointment_edit',
    'appointment_delete',
]


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--clear',
            action='store_true',
            help='clear old states and cities',
        )

    def handle(self, *args, **options):
        if options['clear']:
            Role.objects.all().delete()
            self.stdout.write(self.style.SUCCESS(f"CLEAR"))

        role, created = Role.objects.update_or_create(name='دکتر', code=ROLE_CODES.DOCTOR)
        role.permissions.set(Permission.objects.filter(code__in=DOCTOR_PERMS))

        role, created = Role.objects.update_or_create(name='کارمند', code=ROLE_CODES.STAFF)
        role.permissions.set(Permission.objects.filter(code__in=STAFF_PERMS))

        self.stdout.write(self.style.SUCCESS(f"DONE..."))
