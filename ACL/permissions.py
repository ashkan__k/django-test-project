PERMISSIONS = []

USERS_PERMISSIONS = {
    'title': 'دسترسی کاربران',
    'permissions': [
        {'name': 'لیست کاربران', 'code': 'user_list', 'description': 'دسترسی لیست کاربران'},
        {'name': 'افزودن کاربر', 'code': 'user_create', 'description': 'دسترسی ساخت کاربر جدید'},
        {'name': 'ویرایش کاربر', 'code': 'user_edit', 'description': 'دسترسی ویرایش کاربران'},
        {'name': 'حذف کاربر', 'code': 'user_delete', 'description': 'دسترسی حذف کاربران'},
        {'name': 'تغییر رمز عبور کاربر', 'code': 'user_change_password',
         'description': 'دسترسی تغییر رمز عبور کاربران'},
    ]
}
PERMISSIONS.append(USERS_PERMISSIONS)

######################################################################

ROLES_PERMISSIONS = {
    'title': 'دسترسی نقش ها',
    'permissions': [
        {'name': 'لیست نقش ها', 'code': 'role_list', 'description': 'دسترسی لیست نقش ها'},
        {'name': 'افزودن نقش', 'code': 'role_create', 'description': 'دسترسی ساخت نقش جدید'},
        {'name': 'ویرایش نقش', 'code': 'role_edit', 'description': 'دسترسی ویرایش نقش ها'},
        {'name': 'حذف نقش', 'code': 'role_delete', 'description': 'دسترسی حذف نقش ها'},
    ]
}
PERMISSIONS.append(ROLES_PERMISSIONS)

######################################################################

APPOINTMENT_PERMISSIONS = {
    'title': 'دسترسی نوبت دهی',
    'permissions': [
        {'name': 'لیست نوبت دهی', 'code': 'appointment_list', 'description': 'دسترسی لیست نوبت دهی'},
        {'name': 'افزودن نوبت دهی', 'code': 'appointment_create',
         'description': 'دسترسی ساخت نوبت دهی جدید'},
        {'name': 'ویرایش نوبت دهی', 'code': 'appointment_edit', 'description': 'دسترسی ویرایش نوبت دهی'},
        {'name': 'حذف نوبت دهی', 'code': 'appointment_delete', 'description': 'دسترسی حذف نوبت دهی'},
    ]
}
PERMISSIONS.append(APPOINTMENT_PERMISSIONS)


######################################################################

TEST_RESULT_PERMISSIONS = {
    'title': 'دسترسی جواب آزمایش',
    'permissions': [
        {'name': 'لیست جواب آزمایش', 'code': 'test_result_list', 'description': 'دسترسی لیست جواب آزمایش'},
        {'name': 'افزودن جواب آزمایش', 'code': 'test_result_create',
         'description': 'دسترسی ساخت جواب آزمایش جدید'},
        {'name': 'ویرایش جواب آزمایش', 'code': 'test_result_edit', 'description': 'دسترسی ویرایش جواب آزمایش'},
        {'name': 'حذف جواب آزمایش', 'code': 'test_result_delete', 'description': 'دسترسی حذف جواب آزمایش'},
    ]
}
PERMISSIONS.append(TEST_RESULT_PERMISSIONS)

######################################################################

PAYMENTS_PERMISSIONS = {
    'title': 'دسترسی تراکنش ها',
    'permissions': [
        {'name': 'لیست تراکنش ها', 'code': 'payments_list', 'description': 'دسترسی لیست تراکنش ها'},
    ]
}
PERMISSIONS.append(PAYMENTS_PERMISSIONS)

######################################################################

COMMENTS_PERMISSIONS = {
    'title': 'دسترسی نظرات',
    'permissions': [
        {'name': 'لیست نظرات', 'code': 'comments_list', 'description': 'دسترسی لیست نظرات'},
        {'name': 'تغییر وضعیت نظرات', 'code': 'comments_change_status', 'description': 'دسترسی تغییر وضعیت نظرات'},
        {'name': 'حذف نظرات', 'code': 'comments_delete', 'description': 'دسترسی حذف نظرات'},
    ]
}
PERMISSIONS.append(COMMENTS_PERMISSIONS)


class ROLE_CODES:
    DOCTOR = "doctor"
    STAFF = "staff"
